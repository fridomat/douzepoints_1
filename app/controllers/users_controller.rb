class UsersController < ApplicationController
  before_action :logged_in_user, only: [:show]
  before_action :correct_user,   only: [:show]

	def index
		@users = User.all
		@countries = Country.all.sort {|a,b| a.total_score <=> b.total_score}.reverse
	end

	def show
		@user = User.find(params[:id])
		possible_points = (0..12).to_a - [9] - [11]

		@points_options = []
		possible_points.each do |value|
			@points_options.push([value, value])
		end

		@countries = Country.all

		checks = Hash.new

		@countries_for_table = []

		@countries.each do |country|
			country_for_row = Hash.new
			country_for_row["country"] = country
			country_for_row["vote"] = country.votes_by_user(@user)

			points = country_for_row["vote"].points

			if points == 12
				checks[12] = country
			elsif points == 10
				checks[10] = country
			elsif points == 8
				checks[8] = country
			end

			@countries_for_table.push(country_for_row)
		end


		@countries_for_table.each do |c|
			row_possible_points = possible_points.clone

			if checks[12] && checks[12] != c["country"]
				row_possible_points = row_possible_points - [12]
			end

			if checks[10] && checks[10] != c["country"]
				row_possible_points = row_possible_points - [10]
			end

			if checks[8] && checks[8] != c["country"]
				row_possible_points = row_possible_points - [8]
			end

			c["points_options"] = row_possible_points
		end
	end

	 def new
	  	@user = User.new
	  end

	def create  
	  	@user = User.new(user_params)  
	  	if @user.save
	      log_in @user 
	  		flash[:success] = "Welcome to 12points!"
	  		redirect_to @user
	  	else	
	  		render 'new'
	  	end
  	end

  	private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

end

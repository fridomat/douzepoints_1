class CountriesController < ApplicationController



	def index
		@countries = Country.all
		@countries = Country.all.sort {|a,b| a.total_score <=> b.total_score}.reverse
	end

	def show
		@country = Country.find(params[:id])
	end
end

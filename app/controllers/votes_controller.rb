class VotesController < ApplicationController	

  def create
    u = User.find(params[:user_id])
    v = u.votes.create(params.require(:vote).permit(:points, :country_id))
    redirect_to user_path(u)
  end

  def update
    vote = Vote.find(params[:id])
    u = User.find(params[:user_id])

    if vote.update_attributes(vote_params)
      flash[:success] = "Punktzahl korrigiert."
      redirect_to user_path(u)
    else
      flash[:error] = "Punktzahl konnte nicht korrigiert werden."
      render action: :edit
    end 
  end

  private 

  def vote_params
    params[:vote].permit(:points, :user_id)
  end

end
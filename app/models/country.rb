class Country < ActiveRecord::Base
	has_many :votes
	has_many :users, through: :votes

	def total_score
	  votes.sum(:points)
	end

	def votes_by_user(user)
		Vote.find_by(user: user, country: self) || Vote.new(:points => 0)
	end
end
 